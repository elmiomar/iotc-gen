import password

ORIGINAL_PASSWORD = "ThisISMyPasSWorD"
ENCODED_PASSWORD = b"VGhpc0lTTXlQYXNTV29yRA=="

def test_password_encoding():
    encoded = password.encode_password(ORIGINAL_PASSWORD)
    assert encoded == ENCODED_PASSWORD


def test_password_decoding():
    decoded = password.decode_password(ENCODED_PASSWORD)
    assert decoded == ORIGINAL_PASSWORD