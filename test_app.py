import json
import pickle
from click.testing import CliRunner, Result
from app import main

json_object1 = """{
    "broker": {
        "address": "127.0.0.1",
        "port": 8883,
        "username": "roger",
        "password": "cGFzc3dvcmQ=",
        "certificate": "certs/ca.crt"
    },
    "communication": "MQTT",
    "hardware": "GrovePi",
    "sensors": []
}
"""

json_dict1 = {
    "broker": {
        "address": "127.0.0.1",
        "port": 8883,
        "username": "roger",
        "password": "cGFzc3dvcmQ=",
        "certificate": "certs/ca.crt"
    },
    "communication": "MQTT",
    "hardware": "GrovePi",
    "sensors": [
        {
            "type": "temperature",
            "pin": 7
        },
        {
            "type": "humidity",
            "pin": 7
        }
    ]
}

json_dict1_str = """{
    "broker": {
        "address": "127.0.0.1",
        "port": 8883,
        "username": "roger",
        "password": "cGFzc3dvcmQ=",
        "certificate": "certs/ca.crt"
    },
    "communication": "MQTT",
    "hardware": "GrovePi",
    "sensors": [
        {
            "type": "temperature",
            "pin": 7
        },
        {
            "type": "humidity",
            "pin": 7
        }
    ]
}
"""

def test_input_json():
    """
    Test configuration through json input file.
    """
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("iot-component.json", "w") as f:
            f.write(json_object1)
        
        result = runner.invoke(main, ["--json", "iot-component.json"])
        assert result.exit_code == 0
        assert "Loaded configuration successfully." in result.output
        assert json_object1 in result.output


def test_output_json():
    """
    Test output configuration into json file.
    """
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(main, ["-s", "temperature", "7", "-s", "humidity", "7", "--json-output", "iot-component.json"])
        assert result.exit_code == 0
        assert "Saving configuration to json file" in result.output
        
        f = open("iot-component.json", "r")
        json_dictionary = json.load(f)
        f.close()
        
        assert json_dict1 == json_dictionary


def test_input_pickle():
    """
    Test configuration through pickle input file.
    """
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("iot-component.pickle", "wb") as f:
            pickle.dump(json_dict1, f)
        
        result = runner.invoke(main, ["--file", "iot-component.pickle"])
        assert result.exit_code == 0
        assert "Loaded configuration successfully." in result.output
        assert json_dict1_str in result.output


def test_output_pickle():
    """
    Test output configuration into pickle file.
    """
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(main, ["-s", "temperature", "7", "-s", "humidity", "7", "--output", "iot-component.pickle"])
        assert result.exit_code == 0
        assert "Saving configuration to pickle file" in result.output
        f = open("iot-component.pickle", "rb")
        objects = []
        while True:
            try:
                objects.append(pickle.load(f))
            except EOFError:
                break
        f.close()
        assert json_dict1 == objects[0]
