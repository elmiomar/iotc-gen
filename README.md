# iotc-gen

CLI app for generating IoT Components configuration.


### Download

```
git clone https://gitlab.com/elmiomar/iotc-gen.git
```

### Insatll requirements 

Make sure you have python and pip installed.

```
cd iotc_gen
pip install -r requirements.txt
```

### Usage

- Show help message
  
```
python app.py --help
```

- Generate an IoT Component config

```
python app.py --sensor temperature 7 --sensor humidity 7
```

Running this command will generate two files: a pickle file `iot-component.pickle`, and a json file `iot-component.json` in the current directory.

In this example the content of the json file looks like the following:

```json
{
    "broker": {
        "address": "127.0.0.1",
        "port": 8883,
        "username": "roger",
        "password": "password",
        "certificate": "certs/ca.crt"
    },
    "communication": "MQTT",
    "hardware": "GrovePi",
    "sensors": [
        {
            "type": "temperature",
            "pin": 7
        },
        {
            "type": "humidity",
            "pin": 7
        }
    ]
}
```

- Load existing config file

```
python app.py --file iot-component.pickle

```