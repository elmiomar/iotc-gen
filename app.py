import json
import pickle
import click
import password as pwd

class BorkerConfiguration:
    """
    A class for holding the broker configuration.

    Attributes
    ----------
    address: str
        address of the broker
    port: int
        port of the broker
    username: str
        username used to connect to the broker
    password: str
        password used to connect to the broker
    certificate: str
        certificate authority used to securily connect to the broker
    """
    def __init__(self, address, port, username, password, certificate):
        """
        Constructs all the necessary attributes for the BorkerConfiguration object.
        """
        self.address = address
        self.port = port
        self.username = username
        self.password = pwd.encode_password(password).decode("utf-8") # the decode("utf-8") is to remove the b
        self.certificate = certificate
    

# Default broker to be used in case none was provided
DEFAULT_BROKER = BorkerConfiguration("127.0.0.1", 8883, "roger", "password", "./ca.crt")

class IoTComponentConfiguration:
    """
    A class for holding the IoT Component configuration.

    Attributes
    ----------
    broker: Broker
        the broker to connect to
    communication: str
        communication protocol used to connect to server, e.g. MQTT, CoAP
    hardware: str
        hardware used to interface with sensors, e.g. GrovePi, SenseHat
    sensors: array
        list of sensors
    """

    def __init__(self, broker=DEFAULT_BROKER, communication="MQTT", hardware="GrovePi", sensors = []):
        """
        Constructs all the necessary attributes for the IoTComponentConfiguration object.
        """
        self.broker = broker
        self.communication = communication
        self.hardware = hardware
        self.sensors = sensors
    
    def __str__(self):
        """
        String representation of the IoTComponentConfiguration.
        """
        config_dict = {
            "broker": {
                "address": self.broker.address,
                "port": self.broker.port,
                "username":self.broker.username,
                "password":self.broker.password,
                "certificate": self.broker.certificate
            },
            "communication": self.communication,
            "hardware": self.hardware,
            "sensors": self.sensors
        }
        return json.dumps(config_dict, indent=4)

    def from_pickle(self, input):
        """
        Constructs an IoTComponentConfiguration from an existing pickle configuration file.

        Parameters
        ----------
        input: str
            file path to the pickle configuration file.
        """
        pickle_file = open(input, "rb")
        objects = []
        while True:
            try:
                objects.append(pickle.load(pickle_file))
            except EOFError:
                break
        pickle_file.close()
        config_dict = objects[0]
        broker = BorkerConfiguration(config_dict["broker"]["address"], config_dict["broker"]["port"], config_dict["broker"]["username"], pwd.decode_password(config_dict["broker"]["password"]), config_dict["broker"]["certificate"])
        self.broker = broker
        self.communication = config_dict["communication"]
        self.hardware = config_dict["hardware"]
        self.sensors = config_dict["sensors"]

    def from_json(self, input):
        """
        Constructs an IoTComponentConfiguration from an existing json configuration file.

        Parameters
        ----------
        input: str
            file path to the json configuration file.
        """
        file = open(input)
        config_dict = json.load(file)
        file.close()
        broker = BorkerConfiguration(config_dict["broker"]["address"], config_dict["broker"]["port"], config_dict["broker"]["username"], pwd.decode_password(config_dict["broker"]["password"]), config_dict["broker"]["certificate"])
        self.broker = broker
        self.communication = config_dict["communication"]
        self.hardware = config_dict["hardware"]
        self.sensors = config_dict["sensors"]

    def dump_pickle(self, output):
        """
        Stores the IoTComponentConfiguration object in a pickle file.

        Parameters
        ----------
        output: str
            file path to the output pickle configuration file.
        """
        print(f"Saving configuration to pickle file {output}")
        config_dict = {
            "broker": {
                "address": self.broker.address,
                "port": self.broker.port,
                "username":self.broker.username,
                "password":self.broker.password,
                "certificate": self.broker.certificate
            },
            "communication": self.communication,
            "hardware": self.hardware,
            "sensors": self.sensors
        }
        with open(output, "wb") as file:
            pickle.dump(config_dict, file)

    def dump_json(self, output):
        """
        Stores the IoTComponentConfiguration object in a json file.

        Parameters
        ----------
        output: str
            file path to the output json configuration file.
        """

        print(f"Saving configuration to json file {output}")
        config_dict = {
            "broker": {
                "address": self.broker.address,
                "port": self.broker.port,
                "username":self.broker.username,
                "password":self.broker.password,
                "certificate": self.broker.certificate
            },
            "communication": self.communication,
            "hardware": self.hardware,
            "sensors": self.sensors
        }
        with open(output, "w") as file:
            json.dump(config_dict, file, indent=4) # use indent=4 to pretty print the configuration


@click.command()
@click.option("--file", "-f", type=str, help="Specify the filename of an existing IoT component. If this file is provided any of the below parameters will be ignored.")
@click.option("--json", "-j", type=str,help="Specify json file of an existing IoT component. If this file is provided any of the below parameters will be ignored.")
@click.option("--broker-address", "-a", type=str, help="Specify the broker's address.", default="127.0.0.1")
@click.option("--broker-port", "-p", type=int, help="Specify the broker's port.", default=8883)
@click.option("--broker-username", "-u", type=str, help="Specify the client's username within the broker.", default="roger")
@click.option("--broker-password", "-w", type=str, help="Specify the client's password within the broker.", default="password")
@click.option("--certificate-path", "-t", type=str, help="Specify the path to the ca certificate. If not provided, this will expect a ceritificate with name 'ca.crt' in directory called 'certs'.", default="certs/ca.crt")
@click.option("--communication", "-c", type=click.Choice(["MQTT", "COAP", "WS"], case_sensitive=False), help="Specify which protocol the sensor(s) will be using.", default="MQTT")
@click.option("--hardware", "-h", type=click.Choice(["GrovePi", "SenseHat"], case_sensitive=False), help="Specify which hardware the sensor(s) will be using.", default="GrovePi")
@click.option("--sensor", "-s", multiple=True, type=(str, int), help="Specify sensor details, type and pin.")
@click.option("--output", "-o", type=str, help="Specify the pickle output file where the IoT component config will be stored for later use.")
@click.option("--json-output", "-O", type=str, help="Specify the json output file where the IoT component config will be stored for later use.")
def main(file, json, broker_address, broker_port, broker_username, broker_password, certificate_path, communication, hardware, sensor, output, json_output):
    """
    A tool for generating an IoT Component configuration. The tool allows the user to load an existing configuration from a pickle or from a json file.
    If an input file is provided, there is no need to specify the various options. If no input file is provided, then the user can speicify the different options.
    If no options were speicified, the tool will use the default value for each option.
    When creating a new configuration using the options, the user can output the resutling configuration into a file for later use.
    
    Example:\n
    python app.py --broker-address 192.168.1.250 --broker-port 8883 \
        --communication MQTT --sensor temperature 7 --sensor humidity 7 \
        --json-output iot-component.json
    
    """
    
    iot_component_config = IoTComponentConfiguration()
    if file is not None:
        iot_component_config.from_pickle(file)
        print(f"Loaded configuration successfully.\n{iot_component_config}")
    elif json is not None:
        iot_component_config.from_json(json)
        print(f"Loaded configuration successfully.\n{iot_component_config}")
    else:
        broker = BorkerConfiguration(broker_address, broker_port, broker_username, broker_password, certificate_path)

        # unpacking sensors in an array to use in the config object below
        sensors_array = []
        for s in sensor:
            sensors_array.append({"type":s[0], "pin":s[1]})
        
        iot_component_config.broker = broker
        iot_component_config.communication = communication
        iot_component_config.hardware = hardware
        iot_component_config.sensors = sensors_array

        if output is not None:
            iot_component_config.dump_pickle(output)
        if json_output is not None:
            iot_component_config.dump_json(json_output)
        print(iot_component_config)


if __name__ == "__main__":
    main()
