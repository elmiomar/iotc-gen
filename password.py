import base64

def encode_password(password):
    """
    Function to hide password using base64 encoding. 
    This serves just an obfuscation purose, so password can look geberrish when imported into files.

    Args:
        password ([string]): password to obfuscate

    Returns:
        [string]: obfuscated password
    """
    encoded = ""
    if password is not None:
        encoded = base64.b64encode(password.encode("utf8"))
    return encoded

def decode_password(hash):
    """
    Function to decode the obfuscated password.

    Args:
        hash ([string]): the base64 encoding of the password

    Returns:
        [string]: original password
    """
    decoded = ""
    if hash is not None:
        decoded = base64.b64decode(hash).decode("utf8")
    return decoded
